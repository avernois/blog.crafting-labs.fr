---
layout: page
title: "Le dojo Crafting Labs"
permalink: /dojo/
---

[![three persons pointing at a silver laptop by John Schnobrich](/assets/images/john-schnobrich-2FPjlAyMQTA-320px.jpg)](https://unsplash.com/photos/2FPjlAyMQTA){: .left-image}
> Le dojo (道場, dōjō?, [doːdʑoː]) [...]. Littéralement en japonais, dō signifie la voie (c'est le même caractère que le tao chinois), le dōjō est le lieu où l'on étudie/cherche la voie.. -- [Wikipédia](https://fr.wikipedia.org/wiki/Dojo)

La voie du code, ça fait un peu présomptueux. En même temps, apprendre à écrire du code maintenable est une quête sans fin, Je ne crois pas qu'il y ait un moment où l'on puisse se dire qu'on est arrivé au bout de la route et qu'il n'y a plus rien à faire pour progresser.


# La proposition

## des sessions régulières

En petit groupe de deux ou trois, stable, on prend un problème prétexte, un kata, et on le résout ensemble. 

L'objectif est de prendre du recul autant sur les pratiques (test, tdd, code maintenable, efficacité, ...) que sur les mécanismes pour aborder les différents problèmes qui s'offriront à nous. Et c'est là mon rôle avec vous, vous orienter dans cette prise de recul et vous offrir des pistes pour alimenter votre réflexion.

Les sessions s'organisent en périodes de plus ou moins 10 semaines. Au cours d'une période, toutes les sessions ont le même problème comme point de départ, avec de temps à autres des variations, autant dans l'espace du problème[^probleme] que dans celui des solutions[^solution].

Généralement, on codera en java, mais c'est juste pour avoir un support, la connaissance du langage n'est pas nécessaire. La plupart du temps, je m'occuperai de taper le code : le groupe s'occupe de la réflexion de haut niveau et je me chargerai des faire la traduction :)

Les sessions durent 90 minutes.

## pour qui ?

Le dojo est pour tout⋅e⋅s les développeu⋅r⋅se⋅s qui souhaitent progresser, prendre du recul ou réfléchir sur leurs pratiques en pratiquant et échangeant avec d'autres. Il s'adresse autant aux grand⋅e⋅s débutant⋅e⋅s qu'aux plus ancien⋅ne⋅s.

On évoluera essentiellement dans un environnement "orienté objet", mais la connaissance de ce type d'approche n'est pas un prérequis, et la plupart de ce qu'on abordera devrait se transposer dans d'autres paradigmes.

Si ça te tente mais que tu as un doute, n'hésite pas [à me contacter](/contact) pour qu'on en discute.


## un espace ouvert

Le dojo, au travers des sessions, est avant tout un espace d'échange et d'apprentissage.

Je porte une attention particulière à ce que tout⋅e⋅s les participant⋅e⋅s se sentent bienvenu⋅e⋅s et puissent évoluer librement indépendamment de leur genre, expérience, langage de prédilection, origine, couleur de peau, orientation sexuelle, religion, liste non exhaustive.

# Inscription

Si ça vous intéresse, il suffit de m'envoyer un petit message, par [twitter @avernois](https://twitter.com/avernois) (dm ouvert) ou via le [formulaire de contact](/contact).

La prochaine période commencera la semaine du 21 avril 2021 et se terminera la dernière semaine de juin. Selon les places disponible dans les groupes existants, il est possible de rejoindre en cours de route[^rejoindre].

# Prix

Le prix est libre. Après 2 sessions, je demande aux participant⋅e⋅s de répondre à deux questions : 
* s'il⋅elle souhaite continuer ?
* combien il⋅elle souhaite payer ? 

La page [Coding dojo, combien ça coûte ?](/dojo/prix-libre/) vous donne des conseils pour choisir le montant.

En résumé, pour une periode de 10 semaines, je conseille :
* c'est toi, personnellement qui va payer : prix libre, conseillé 250€ HT[^echeance]. 
* tu es indépendant⋅e⋅s, ou dans une petite structure avec peu de moyens : prix libre, minimum conseillé 500€ HT[^echeance].
* ta société verse des dividendes à ses investisseurs, tu es blindé de thunes ou ta start-up a fait une grosse levée de fond : prix libre, minimum conseillé 1000€ HT[^echeance].


# En pratique

Les sessions sont entièrement à distance, actuellement on utilise [discord](https://discord.com/) pour l'audio et le partage d'écran, pas de video/webcam.

Un casque avec micro est fortement recommandé.


# Histoire du dojo

La première période a eu lieu à l'été 2020. Il a été annoncé dans [ce billet]({% post_url 2020-05-27-coding_dojo_regulier %}). À la fin de la période, j'ai fait un [état de ce qui s'y est passé]({% post_url 2020-09-22-dojo-feedback %}).

---
Crédits photos :
* three persons pointing at a silver laptop by [John Schnobrich on Unsplash](https://unsplash.com/photos/2FPjlAyMQTA)

[^echeance]: on peut imaginer que ce montant soit payé en plusieurs fois. Il n'y a pas vraiment de portes fermées :)
[^rejoindre]: mais au delà des 2 ou 3 première sessions, cela peut être difficile.