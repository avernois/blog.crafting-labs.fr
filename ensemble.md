---
layout: page
title: Travaillons ensemble
permalink: /ensemble/
---

## Qui suis-je ?

Je m'appelle Antoine Vernois et je suis développeur logiciel avec plus de 20 ans d'expérience.

Depuis une quinzaine d'années je me concentre, en plus de répondre aux besoins fonctionnels, sur la maintenabilité du code produit et passer mes connaissances et pratiques aux équipes avec lesquelles je travaille.

Après plusieurs années dans différents environnements, en 2012 je deviens indépendant et crée Crafting Labs.

Depuis, je suis intervenu dans des équipes de toutes tailles et tous contextes sur des bases de code en tous langages[^langage].

## Que fais-je ?

Mon axe principal est d'écrire et/ou rendre maintenable le code nécessaire pour répondre aux besoins des utilisateur⋅rice⋅s.
L'angle de départ est souvent technique (problème de couplage, architecture inadaptée, tests déficients) mais dérive très vite sur l'organisation de l'équipe et son entourage, la [loi de Conway]({% post_url 2009-04-02-loi-de-conway %}) à l'oeuvre.

J'interviens principalement de deux façons :
* intégré dans une équipe de dev:
    * je deviens un membre de l'équipe à part entière [plus de détails](#intégré-à-léquipe)
    * 3 à 4 jours par semaine pendant 3 à 24 mois[^duree].
* en accompagnement 
    * je guide l'équipe (et la structure dans laquelle elle se trouve) pour faire évoluer ses pratiques aussi bien techniques qu'organisationnelles [plus de détails](#en-accompagnement)
    * de quelques heures par semaine à quelques heures par mois

Depuis 2018, je travaille exclusivent à distance ("en remote"). 
Cela dit, des déplacements ponctuels sont tout à fait possible[^deplacement], particulièrement en début d'intervention pour faire connaissance.

### Intégré à l'équipe

Je deviens membre à part entière de l'équipe et, avec les autres membres, je travaille à l'évolution du produit.
Et j'en profite pour proposer les changements nécessaires par faciliter sa maintenance et son évolutiion.

Je pose deux contraintes pour ce mode de fonctionnement :
* La première porte sur l'organisation de l'équipe : il faut qu'il y ait une équipe qui ait envie de travailler ensemble (même si elle ne le fait pas encore) : on partage régulièrement sur ce qui se passe au sein de l'équipe et les objectifs sont communs. Aussi, les développement que je réalise se font en binôme avec un·e membre de l'équipe[^binome].
* La seconde est sur l'objectif : je ne viens pas juste comme une force de production supplémentaire. Mon objectif est de vous aider à rendre votre code maintenable. Même si je prône de ne jamais arrêter le developpement de fonctionnalités, reorganiser du code prend du temps, et réoganiser une équipe entraine souvent des dysfonctionnement temporaires avant de gagner en efficacité.

### En accompagnement

L'accompagnement peut prendre des formes variées pour s'adapter au mieux des besoins du groupe accompagné.
J'ai une préférence pour les accompagnements de l'équipe dans son intégralité (dev, responsable produit, management) parce que la communication et la collaboration entre l'ensemble des intervenant.e.s est généralement une clé importante pour résoudre les problèmes techniques.

Le plus souvent se sont des sessions en groupe de une à deux heures. 
Partant d'un état des lieux du système, on essaie ensemble de dégager les éléments problématiques (techniques ou organisationels) et explorer l'espace des solutions et leurs implications.

## D'autres possibilités

Si je travaille principalement à moyen/long terme avec une équipe, intégré ou en accompagnement, j'ai aussi des petits formats plus ponctuels.

### Phorésie

Au lieu de venir chez vous développer une application, vous me suivez une journée à développer une des miennes.

J'ai détaillé le concept plus amplement dans [ce billet](/2018/01/16/phoresie/).

* durée : 1 journée. répétable.
* audience : toute personne ayant envie d'écrire du code. 1 personne par session.
* lieu : à distance ou là où je me trouve

### Mob-programming

Un peu comme phorésie, mais dans l'autre sens. Et en groupe : votre code, un vidéo projecteur, une fonctionnalité à développer et on y va tous ensemble.

Tous les détails dans le billet dédié [Une formation les mains dedans](/2015/03/17/une-formation-les-mains-dedans).

* durée : 2 à 5 jours + 1 jour de préparation préalable[^3]
* audience : une équipe de développement[^2]. 6 personnes max.
* lieu : dans vos locaux ou un lieu réservé pour l'occasion

### Mentoring

Une grande partie du métier de développeur⋅euse ne s'apprend pas sur les bancs de l'école mais sur le terrain, par la pratique et l'expérience.
Pour que cela se passe dans de bonnes conditions, il est souvent préférable d'être accompagné par des gens qui ont plus d'expériences qui peuvent indiquer les pièges ou les chemins plus sûrs.

Parfois, on se retrouve dans un environnement dans lequel ces personnes d'expériences ne sont pas là, pas disponibles, ou pas intéressées pour tenir ce rôle.

C'est là que j'interviens.

* durée : un point hebdomadaire et un support permanent (mais asynchrone).
* audience : développeur⋅euse ayant envie de progresser. Session individuelle.
* lieu : à distance

Tous les détails sur la page dédiée [Mentoring](/mentoring).

### Code Retreat

Une journée pour s'ouvrir l'esprit, essayer des choses et prendre du recul sur ses pratiques quotidiennes de développement.

Voir le billet écrit à l'occasion du [Global Day of CodeRetreat 2012](/2011/11/12/code-retreat-toulouse-3-decembre/)

* durée : 1 journée
* audience : développeuse.eur.s. Entre 8 et 16 participant.e.s.
* lieu : dans vos locaux ou un lieu réservé pour l'occasion


### Conseil à la demande

Des fois, on a juste besoin d'un regard extérieur pour mettre en perspective une situation, trouver d'autres angles pour aborder un sujet, débloquer un sujet.
Prenez un rendez vous d'une heure en ligne rapidement, et discutons-en.

Dans [binôme à la demande](/2018/03/08/binome_a_la_demande/) je décris le principe. Le billet est très orienté développement[^11], mais ce n'est pas une restriction du sujet.

C'est aussi une bonne entrée en matière si vous envisagez de me faire intervenir plus régulièrement dans votre contexte.

* durée : de 45min à 2h
* audience : toute personne impliquée dans le développement logiciel.
* lieu : à distance


### Fabrication

J'aime fabriquer des choses qui ont une concrétisation physique, des objets que l'on peut toucher.
Depuis quelques temps ces objets prennent la forme de joyeux mélanges d'électronique, de LEDs, de bois et d'acrylique. Comme par exemple [Hexagon](https://shop.crafting-labs.fr/fr/products/hexagon/) ou [Binary A](https://shop.crafting-labs.fr/fr/products/binary_a/)[^9].

Par exemple, on pourrait imaginer et réaliser ensemble un indicateur de build à l'image de votre équipe/produit/autre.

note: il ne s'agit pas de construire des produits à produire en masse. On parle d'objet unique ou micro série (max 10 ?) produit à la main[^10].

## Contact

On peut me joindre par e-mail via ce [formulaire de contact](/contact/) ou sur mastodon [@avernois@piaille.fr](https://piaille.fr/@avernois) (mes dm sont ouverts).

-----

[^1]: ou pas.
[^2]: product owner, ops et testeuses.eurs font évidemment partie de l'équipe de développement.
[^3]: dont l'objectif est de me familiariser un peu avec le code et l'environnement de travail. C'est optionnel, mais fortement conseillé.
[^4]: ou deux, probablement pas plus de trois.
[^5]: sur au moins un an. Voir plus.
[^6]: et pourtant, c'est presque dernier groupe dont je parle ? trouverez vous pourquoi ?
[^7]: je me demande même si ce n'est pas celles que je préfère. Surprenez moi :)
[^8]: un énorme merci à [Ludo](https://twitter.com/ludopradel) qui m'a mis sur la voie.
[^9]: yep, je suis très branché horloge.
[^10]: en tout cas, c'est là où moi je m'arrêterai. Si après vous voulez prendre les design auxquels ont est arrivé et en faire des lots, libre à vous :)
[^11]: c'est de là que vient l'idée.
[^12]: et par vous, j'entends toute l'organisation impliquée
[^13]: de quelques jours par semaine à quelques jours par mois selon votre avancée.
[^14]: mais ça vous demandera toujours du temps et des efforts continus.
[^duree]: la limite haute de 24 mois est arbitraire. Il n'y a techniquement rien qui m'empêcherait de rester plus longtemps si cela faisait sens.
[^langage]: de JavaScript à Java en passant par PHP, C#, Claire, ...
[^deplacement]: voir encouragé :)
[^binome]: de préférence, pas toujours la même personne.