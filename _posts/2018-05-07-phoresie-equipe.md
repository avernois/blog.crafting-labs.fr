---
layout: post
title: "Phorésie en équipe"
lang: fr
date: 2018-05-07 12:30:00 +0200
categories: 
- "code"
- "apprendre"
- "partager"
- "équipe"
---


[![By Albert kok [CC BY SA 3.0 (https://creativecommons.org/licenses/by-sa/3.0/deed.en)], via Wikimedia Commons](https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Lemonshark.jpg/320px-Lemonshark.jpg)](https://upload.wikimedia.org/wikipedia/commons/9/93/Lemonshark.jpg){: .left-image}

> La phorésie (du grec -φόρος, -phoros, de φέρω, pherô, « porter ») est un type d'interaction entre deux organismes où un individu (le phoronte) est transporté par un autre (l'hôte). -- [Wikipédia](https://fr.wikipedia.org/wiki/Phor%C3%A9sie)

Continuons les expérimentations autour du format phorésie. Maintenant, je vous propose de le faire en petit groupe.


# C'est quoi phorésie en équipe ?

C'est comme [phorésie]({% link _posts/2018-01-16-phoresie.md %}), mais en équipe ou petit groupe avec un maximum de 6 personnes.

L'idée de base est de renverser le fonctionnement habituel de la formation ou de l'accompagnement : au lieu de venir dans votre contexte pour apprendre et réfléchir à comment vous pourriez faire, je vous invite dans le mien pour vous montrer comment je fais.

Sur une journée, je prends une vraie fonctionnalité d'une application que je suis en train de développer[^2] et je la développe en racontant à voix haute la réflexion qui se passe dans ma tête.
Quand il y a besoin ou un intérêt, on met le développement en pause pour rentrer dans les détails.

Et puis, comme j'ai la chance d'avoir un groupe autour de moi, j'en profite pour vous demander votre opinion sur les choix qui s'offre à moi.

# Qu'est-ce qu'on va apprendre ?

C'est difficile à dire à l'avance. Cela est fortement dépendant des centres d'intérêts et/ou de questionnements des participant.e.s.

Dans les sessions individuelles, on a beaucoup parlé de responsabilité, d'architecture mais aussi de tests, d'intégration continue, de mise en production, de gestion de version et bien d'autres sujets. 
J'en parle un peu plus dans le billet [phorésie, un premier bilan]({% link _posts/2018-04-14-phoresie-retour.md %}).

# C'est pour qui ?

Tout le monde.

Dans mon idée, ce serait une équipe de développement. Cela inclut tous les profils qui contribuent à la construction du logiciel : des développeuse.eur.s bien sur, mais aussi des testeuse.eur.s, des UX, des UI ou encore des fonctionnels, et tous les autres.

Le seul réel prérequis c'est d'avoir envie de participer à cette journée.

# Ça aura lieu où ?

Pratiquement n'importe où en France voir en Europe.

Idéalement en dehors de vos locaux pour sortir réellement de votre contexte.
Cela dit, si vous disposez d'une salle équipée[^1] relativement isolée cela pourrait se faire là.

# Combien ça coûte ?

Le tarif pour un groupe de 2 à 6 personnes est de 1000€ HT (1200€ TTC) par jour plus les frais de déplacement si en dehors de Toulouse et de location de salle si besoin.

# Comment on s'inscrit ?

En m'envoyant un message via la page de [contact](/contact) ou [twitter @avernois](https://twitter.com/avernois) (mes dm sont ouverts).

[^1]: confortable pour 7 personnes, lumière naturelle, vue sur l'extérieur et vidéoprojecteur HDMI avec une résolution 1080p mini.
[^2]: open source.