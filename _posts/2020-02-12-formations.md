---
layout: post
title: "Formations"
date:  2020-02-12 12:50:00 +0200
categories: 
- "formations"
- "phorésie"
---


[![photography of school room by Thao Le Hoang](/assets/images/feliphe-schiarolli-hes6nUC1MVc-320px.jpg)](https://unsplash.com/photos/yYSY93clr4w){: .left-image}

Je ne donne plus de formations *classique* depuis quelques années déjà, mais je n'avais jamais formalisé les raisons derrière ce choix.
C'est ce que je fais maintenant.


Par formation *classique*, je pense aux formations de quelques jours consécutifs[^duree] que l'on peut donner ou suivre au cours de sa carrière dans le monde du développement logiciel.
Des formations sur un sujet précis[^sujet], avec un plan et un déroulé préparé et relativement figé, au cours desquelles un⋅e sachant⋅e délivre son savoir à des apprenant⋅e⋅s dans un contexte formel.
une fois la session finie, le⋅la sachant⋅e ne revoit jamais les apprenant⋅e⋅s[^apres]

# le fond 
[![group of martial artists sitting on the ground by Thao Le Hoang](/assets/images/thao-le-hoang-yYSY93clr4w-320px.jpg)](https://unsplash.com/photos/yYSY93clr4w){: .right-image}

Cette absence de suivi me dérange. 
La plupart des sujets que j'aime partager avec d'autres développeu⋅r⋅se⋅s sont des sujets de fonds : écrire du code de qualité, des tests pertinents, prendre du recul sur son travail ne sont pas des choses qui s'acquièrent en 3 jours.

J'aime beaucoup cette citation attribuée à *[Laurent Bossavit](https://twitter.com/morendil)* :
>Si je veux apprendre le Judo, je vais m'inscrire au dojo du coin et y passer une heure par semaine pendant deux ans, au bout de quoi j'aurai peut-être envie de pratiquer plus assidûment.
>
>Si je veux apprendre la programmation objet, mon employeur va me trouver une formation de trois jours à Java dans le catalogue 2004.
>
>Cherchez l'erreur.

En trois jours, c'est vrai on peut apprendre des concepts, des méthodes, des astuces.
Mais ça, c'est la partie facile du job. Ce savoir est nécessaire, mais il n'est pas suffisant une fois de retour, seul⋅e dans son contexte pour savoir comment l'appliquer.

En trois jours on peut apprendre le concept de test unitaire, de TDD, les techniques associées ou les outils qui vont avec.
Mais une fois devant son code, on se rend rapidement compte qu'il y a une grande différence entre connaître les stratégies pour choisir le premier test à écrire et faire ce choix.
Je ne parle même pas de l'implémenter ou de prendre du recul pour savoir si ce choix était pertinent.


# la forme
[![empty chair in theater by Nathan Dumlao](/assets/images/nathan-dumlao-ewGMqs2tmJI-320px.jpg)](https://unsplash.com/photos/ewGMqs2tmJI){: .left-image}

Et puis, il y a la forme : d'un côté un⋅e sachant⋅e, de l'autre des apprenant⋅e⋅s.

Je crois que cette séparation, qu'on le veuille ou non, nous transporte à l'époque de notre formation initiale, des bancs d'école : le⋅la sachant⋅e représente la vérité et même si il⋅elle s'attache à ne faire que des propositions, elles sont souvent interprétées comme la "bonne solution", celle qu'il faudra ressortir pour avoir une bonne note.

Cette idée de vérité se retrouve renforcée par l'apparente facilité avec laquelle les sachant⋅e⋅s déroulent les exemples ou répondent aux problèmes des apprenant⋅e⋅s.
Même quand on connait le travail nécessaire pour préparer une formation, ce que l'on voit, ce qui reste dans l'esprit, c'est la fluidité à l'exécution, pas les dizaines d'heures à préparer cette session encore moins les années d'expérience pour y arriver.
On en ressort avec l'idée que c'est facile, une illusion qui renforce la violence du choc lorsqu'on se heurte à la réalité.

Et puis quand ça fait 10 ou 12 fois qu'on donne la même formation, il devient difficile de tomber sur une question totalement originale :)

# alors on fait quoi ?
[![three persons pointing at a silver laptop by John Schnobrich](/assets/images/john-schnobrich-2FPjlAyMQTA-320px.jpg)](https://unsplash.com/photos/2FPjlAyMQTA){: .right-image}

Vous, ce que vous voulez ! :)

Moi, je vais continuer à expérimenter de nouveaux formats, des approches différentes plus compatible avec ma façon d'apprendre. Plusieurs idées prennent forme doucement :
* [walking dev](http://walkingdev.fr/) : explorer un sujet en explorant une ville.
* [phorésie]({% post_url 2018-01-16-phoresie %}) : une session en temps fini (1 à 3 jours), individuel ou en groupe, où je développe en live une vraie fonctionnalité d'une vraie application en explicitant l'intégralité de mon processus de réflexion.
* [mentoring]({% link mentoring.md %}) : un suivi sur le long terme plus individualisé (même si applicable sur une équipe) mêlant sessions pour débloquer des problèmes que l'on rencontre au quotidien, prise de recul et réflexions plus générales. 

Je tourne aussi autour de l'idée de coding dojo comme lieux d'apprentissage régulier, comme école de développement. L'idée n'est pas encore très claire, mais il y a une forte probabilité que j'en parle plus en détails bientôt :)


---
Crédits:

* photography of school room  by [Feliphe Schiarolli on Unsplash](https://unsplash.com/photos/hes6nUC1MVc)
* group of martial artists sitting on the ground by [Thao Le Hoang on Unsplash](https://unsplash.com/photos/yYSY93clr4w)
* empty chair in theater by [Nathan Dumlao on Unsplash](https://unsplash.com/photos/ewGMqs2tmJI)
* three persons pointing at a silver laptop by [John Schnobrich on Unsplash](https://unsplash.com/photos/2FPjlAyMQTA)


[^sujet]: bien que parfois très vaste. J'ai donné des formations de 3 jours intitulées "ingénierie logicielle".
[^duree]: 1 à 5 jours
[^apres]: sauf peut-être à l'occasion d'une autre formation sur un autre sujet.
[^solution]: celle qu'il fallait ressortir pour avoir une bonne note