---
layout: post
title: "Binomage à distance"
date: 2020-09-15 14:00:00 +0200
---

[![an airplane cockpit by Mael BALLAND](/assets/images/mael-balland-V5hAryReZzo-320px.jpg)](https://unsplash.com/photos/V5hAryReZzo){: .left-image}

De toutes les pratiques que j'utilise dans mon quotidien de développeur, je crois que le binômage, ou pair-programming en anglais, est celle que je préfère.
L'idée est simple, au lieu d'être seul⋅e devant son écran pour résoudre son problème, on s'y colle à deux, en même temps, derrière la même machine.

Avec la généralisation du travail à distance (coucou le coronavirus), je vois certains dire que c'est un frein au binômage.
Alors que de mon point de vue, s'il y a une pratique que je préfère encore plus au binômage, c'est le binômage à distance :)


# le binômage, petit rappel

[![two brown girafe by Wolfgang Hasselmann](/assets/images/wolfgang-hasselmann-yJTe4lNuVnE-320px.jpg)](https://unsplash.com/photos/yJTe4lNuVnE){: .right-image}
Je le disais en intro, le principe qui soutient le binômage, c'est qu'au lieu de s'attaquer à un problème seul, on s'y met à deux.
C'est une pratique super efficace autant pour résoudre des problèmes complexes qu'éviter des erreurs, parfois d'inattention, qui autrement auraient pu prendre des jours avant d'être découvertes et/ou corrigées.

Rapidement, le binômage, par défaut, c'est deux personnes devant la même machine : une personne est au clavier/souris écrivant le code pendant que l'autre est là pour prendre du recul sur ce qui est en train de se faire et ce qui va venir. Les deux communiquant activement. J'aime bien l'analogie du pilote/co-pilote.
 
En présentiel, ça se traduit le plus souvent par deux personnes, un clavier, une souris

# en remote

En remote, c'est toujours deux personnes. Sauf que maintenant chacun⋅e peut avoir son écran, son clavier, sa souris, et son bureau réglé à ses envies/besoins. Et les premiers avantages de la distance apparaissent : plus de larges chaises de bureau qui se heurtent pour être suffisamment en face du même écran, plus de perte de temps pour se souvenir comment on fait un '[' sur le clavier de son⋅sa collègue, ...

Pour reproduire le côté deux derrière le même écran/code il suffit de deux choses : que les deux membres du binôme puissent communiquer et voient le même code. Pour cela j'utilise généralement un outil de conférence en ligne et le système de partage d'écran qui va avec[^zoom]. La personne au clavier partage son écran et l'autre peut suivre ce qui se passe. Et voilà, on peut discuter autour du même code comme si l'on était côte à côte.

Présentiel ou à distance, en pilote, je ne vois pas vraiment de différence, à part le fait de toujours pouvoir utiliser mon clavier/ide/raccourcis.

Par contre en co-pilote, un tout nouveau monde de possibilité s'offre à moi.
Maintenant, je dispose de mon propre environnement et ne suis plus restreint⋅e à regarder uniquement ce que fait l'autre. Je peux choisir où se porte mon attention.
Typiquement, quand je suis dans ce rôle, je peux allez chercher des infos dans le reste du code, vérifier la cohérence entre les plans qu'on a fait, faire une recherche sur un détail de syntaxe pour l'avoir au moment où l'autre en aura besoin.

En tant que co-pilote, j'ai l'impression qu'en présentiel, je suis contraint à regarder la route. À distance, je peux aussi suivre la roadmap, vérifier le gps, anticiper les prochains virages, étudier un itinéraire plus efficace/plus sûr, ...


## d'autres outils

### casque et webcam

De mon point de vue, l'une des clés pour que le binômage fonctionne c'est que la paire communique efficacement de façon synchrone. À distance, ça passe généralement par un canal audio avec uniquement les membres du binôme.

Je trouve plus confortable d'utiliser un casque avec un micro[^casque]. Le casque me demande moins d'effort de concentration pour entendre ce que se dit comparé à des hauts parleurs dont le son est moins directionnel.
Pour le micro, mon expérience est qu'à moins d'investir des sommes importantes dans un micro de table de haute qualité, les micro ouverts (sur le fil du casque, la webcam ou posé sur la table) produisent un son inférieur à ceux porté par le casque juste devant la bouche. Beaucoup moins de bruit de fonds et plus de confort pour la personne qui écoute.

Par contre, je trouve la webcam globalement inutile. Généralement, je ne l'allume que la première fois que je rencontre la personne avec qui je vais binômer et plus jamais ensuite.
En plus, ça fait économiser un paquet de bande passante, pratique quand on n'a pas une grosse connexion :)


### live share
[![black and brown headset near laptop by Petr Macháček](/assets/images/petr-machacek-BeVGrXEktIk-320px.jpg)](https://unsplash.com/photos/BeVGrXEktIk){: .left-image}

Un outil que j'apprécie particulièrement, c'est *Live Share* inclut dans [Visual Studio Code](https://code.visualstudio.com/)[^liveshare]. *Live Share* permet de partager un projet et d'être plusieurs à pouvoir l'éditer simultanément. Alors on peut vite se marcher sur les pieds si on ne s'organise pas correctement mais ça peut aussi être super efficace quand la mécanique est bien huilée.

Par exemple, en tant que co-pilote ça permet de corriger une typo à la volée sans avoir à interrompre son/sa partenaire ou d'aller corriger un détail pendant que l'autre continue sur sa lancée. Et tout un tas d'autres possibilités.

Cela dit, si j'adore *Live Share*, VS Code est parfois un peu limité et dans certain cas, j'aurai tendance à me passer de cette fonctionnalité pour bénéficier de celle d'un éditeur plus évolué. Comme [IntelliJ Idea](https://www.jetbrains.com/fr-fr/idea/) quand je fais du java[^idea].


### dessiner

Aujourd'hui, le seul truc qui me manque, par rapport à du présentiel, c'est la possibilité de faire des petits dessin rapide pour partager/résumer une idée avec mon⋅ma binôme. Il y a bien des outils en ligne, type [Miro](https://miro.com/), qui sont intéressants mais ça reste très laborieux par rapport à un gribouillis sur une feuille de papier. 

J'ai découvert récemment des approches a base de [webcam et miroir](https://johnumekubo.com/2020/09/07/the-pocket-document-camera/) qui me semble une approche super intéressante pour garder la liberté du papier sans avoir un setup monstrueux. Il faut que je prenne le temps de bricoler/tester des trucs autour de ça :)


# Conclusion

La distance n'est pas une bonne excuse pour arrêter de faire du binômage si vous avez envie de le pratiquer. 

De mon point de vue, c'est au contraire une bonne excuse pour s'y mettre, à travailler seul⋅e trop longtemps on peut vite se sentir isolé⋅e et perdre des repères. Travailler en binôme fait qu'on est plus seul⋅e pendant un temps, ça ne fait pas tout, mais c'est déjà ça de pris :)
Le feedback rapide, la possibilité d'avoir quelqu'un avec qui partager ces interrogations sur le travail en cours est un confort facile non négligeable.

Bref, si ça vous tente : Go !

Et si vous êtes tout seul⋅e dans votre équipe, il n'y pas forcement besoin d'un gros budget[^budget] pour que je vienne [binômer avec vous](/contact/) de temps en temps :).

---
Merci à [Ludo](https://twitter.com/ludopradel) pour ses précieuses remarques.

Crédits photos:

* airplane cockpit by [Mael BALLAND on Unsplash](https://unsplash.com/photos/V5hAryReZzo)
* two brown girafe by [Wolfgang Hasselmann on Unsplash](https://unsplash.com/photos/yJTe4lNuVnE)
* black and brown headset near laptop by [Petr Macháček on Unsplash](https://unsplash.com/photos/BeVGrXEktIk)

[^moi]: comme moi :)
[^zoom]: j'utilise [zoom.us](https://zoom.us) avec succès. Depuis quelques mois, j'expérimente discord avec intérêt.
[^liveshare]: et Visual Studio tout court il me semble.
[^idea]: j'ai cru comprendre que JetBrains travaillait sur un équivalent de LiveShare pour sa plateforme. J'ai hâte :)
[^casque]: j'utilise un Rig 500 de chez Plantronics depuis plusieurs années avec satisfaction
[^budget]: mais il faut un budget quand même.