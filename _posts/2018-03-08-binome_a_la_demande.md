---
layout: post
title: "Binôme à la demande"
date: 2018-03-8 16:50:00 +0100
lang: fr
categories: 
- "code"
- "apprendre"
- "partager"
---

[!["while (!succeed=try())"](https://farm8.staticflickr.com/7619/16655328640_9eb1c06dac_n.jpg)](https://www.flickr.com/photos/antoinevernois/16655328640){: .left-image}

Un bon camarade développeur me faisait remarquer récemment qu'il lui manquait parfois d'un point de vue extérieur sur ce qu'il était en train de faire, ou ce qui se passait dans son équipe.

Ce n'est pas la première fois que j'entends ça.

Du coup, je tente une proposition : le binôme On Demand.


Que ce soit pour débloquer un problème de code, prendre du recul, ou avoir un conseil sur une situation, on prend une heure et on en discute, voir mieux, on le fait :).

# Qu'est-ce qu'on peut faire ? De quoi on peut parler ?

Tout ce qui tourne autour du développement logiciel. Ça veut dire le code bien sur, mais aussi tout ce qui tourne autour (l'organisation, l'automatisation, le travail en équipe, ...)

L'idée de départ était de binomer pendant une heure sur une fonctionnalité (ou un refactoring).
Mais on pourrait imaginer que cela puisse aussi prendre la forme d'une revue de code en live, d'une réflexion tableau blanc autour d'un point précis d'architecture, d'une discussion sur les problèmes que vous rencontrez dans votre travail, ...

# Ça se passe où ?

Ça se passe en ligne en visio via skype ou hangout par exemple[^2].

Si, par hasard, vous êtes dans la même ville que moi à ce moment là, on peut aussi envisager de se retrouver dans un café.

# Combien de temps ça dure ?

Je pars avec la cible de faire des sessions d'une heure. Mais je ne serai pas surpris si ça durait un peu plus :)[^7]

Une heure ça passe vite, si on réalise que l'on ne pourra pas finir ensemble ce que l'on a commencé, je m'assurerai que vous ayez suffisamment de matière pour finir sans moi.

# Combien ça coûte ?

100€ HT (120€TTC) la session.

# Ça m'intéresse. Comment on s'organise ?

Vous allez [sur cette page de prise de rendez-vous](https://www.vyte.in/craftinglabs/60) et prenez rendez-vous à un moment où j'apparais disponible[^1]. Ça serait une bonne idée de préciser le sujet que vous voudriez que l'on aborde dans le message[^5].

Dès que je reçois l'invitation, je confirme[^3], et c'est parti.

Note : il faut un compte google/office365 pour prendre rendez vous via cet outil. Si vous n'en avez pas ou n'avez pas envie de vous en servir, pas de soucis, regardez juste l'heure qui vous intéresse et envoyez moi un message via ce [formulaire de contact](/contact/) ou sur twitter [@avernois](https://twitter.com/avernois).

# Tu ne voudrais pas venir une heure dans nos locaux plutôt qu'en ligne ?

C'est tout à fait envisageable. Mais, ce n'est plus le même tarif :)

# J'ai des questions.

Cool, j'ai des réponses. À voir si elles correspondent :)

On peut me joindre par email via ce [formulaire de contact](/contact/) ou sur twitter [@avernois](https://twitter.com/avernois) (mes dm sont ouverts).

-----

[^1]: gardez un peu de marge, il m'arrive de passer quelques heures sans regarder mes mails.
[^2]: tant que ça marche sous linux, ça devrait être ok pour moi
[^3]: la session n'est réellement validé qu'à ce moment là. Même si je fais attention, il peut arriver que mon agenda ne soit pas parfaitement synchronisé.
[^5]: comme ça, je peux commencer à l'avoir en tête
[^7]: dans les limites du raisonnable, probablement pas plus de 1h30