---
layout: post
title: C'est historique
date: 2022-06-27 18:00:00 +0200
---

[![some electric cable on a pole in a street by night](/assets/images/2007.09-ICCP-249-cable.320px.jpg)](/assets/images/2007.09-ICCP-249-cable.jpg){: .left-image}

Depuis une dizaine d'années, je jongle entre missions de dev, intégré dans une équipe, et accompagnement technique pour aider des équipes à progresser dans leurs pratiques.
Ça m'amène à voir 3-5 nouvelles bases de codes différentes par an. Et de ce fait, je me retrouve régulièrement devant un code tordu à poser la question :

> *<< euh, quelqu'un sait pourquoi ce truc à été fait comme ça? >>*

Et dans la myriade des réponses possible, de *<<c'est une bonne question">>* à *<<parce que [insérer ici une excellente explication]>>*, il y en a une qui revient souvent :

> *<< [silence géné] ah. ça. euh. c'est historique [rire géné] >>*


Je dois reconnaître qu'au début, cette réponse me faisait un peu paniquer.
Elle arrive avec le poids du passé, comme une malédiction sur cette partie du code, on évite d'en parler, on le contourne autant qu'on peu pour ne pas avoir a y toucher.


Mais, les malédictions ça n'existe pas[^malediction], ce n'est que du code avec un historique que l'on ne maitrise pas. Rien de bien méchant, ou dangereux la dedans.

Au final, maintenant, c'est une réponse que j'apprécie. 

Déjà parce qu'elle est honnête, on ne cherche pas à se trouver des justifications foireuses, à lancer des écrans de fumée pour détourner l'attention. On assume l'histoire. On assume qu'on ne la connaît pas.

Et puis, dans le silence gêné qui précède/suit le *<< c'est historique >>*, il y a une forme de reconnaissance que c'est effectivement un problème. 
Peu importe la raison, pour laquelle on ne s'y est pas encore attaqué[^raison], admettre qu'on a un problème, c'est la première étape pour le résoudre. Pour moi, c'est la quasi garantit que, même s'il y aura des réticences, j'aurai le support de mes camarades quand arrivera le temps d'y trouver une solution.


Aujourd'hui, les bases de codes avec de l'histoire sont mes préférées[^aide] :)

---

Photos :

* des câbles electriques sur un poteau de nuit. Photo prise à Cluj-Napoca (Roumanie) en 2007 (à l'occasion de la conférence ICCP07)

[^raison]: manque de temps, de solution, de courage, de compétence,...
[^malediction]: du moins, si elle existe, je n'en ai jamais croiser (et je préfèrerai autant que ça continue comme ça :) ou alors peut être que le refactoring est une forme d'exorcisme ;)
[^aide]: et si vous avez des problèmes avec l'histoire de votre base de code, j'ai régulièrement de la dispo pour venir vous donner un coup de main. [Les infos pour me contacter](/contact)