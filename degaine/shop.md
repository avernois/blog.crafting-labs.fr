---
layout: page
title: Dégaine - Un compte à rebours à deux faces
permalink: /degaine/shop/
scripts:
  - https://code.jquery.com/jquery-3.3.1.slim.min.js
  - https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.6/umd/popper.min.js
  - https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/js/bootstrap.min.js
  - https://js.stripe.com/v3/
css:
  - https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css
noindex: true
prix_mini_ttc: 24
prix_mini_ht: 20
prix_conseil_ttc: 72
prix_conseil_ht: 60

---

## Présentation

Dégaine est un compte à rebours à deux faces mêlant structure en bois et électronique.
Il a été conçu pour m'aider à suivre le temps lorsque je donne une conférence ou que j'anime un atelier. 
L'information est dupliquée sur chaque face la rendant plus facilement accessible à l'ensemble des participant⋅e⋅s.

Le temps est configurable de 0[^4] à 99 minutes. Une fois lancé, il indique le temps restant en minutes d'abord puis en secondes pour les dernieres 99 secondes.
Lorsque 50% du temps est écoulé il change de couleur et change encore à 90% puis 100%[^5].

Il s'agit d'un projet en évolution, conçu et assemblé à la main (les miennes)[^8].

Après avoir commandé, si vous le souhaitez, vous pourrez être impliqué dans les choix restant à faire et proposer des changements/évolutions[^10].

L'ensemble du projet est open source sous licence MIT et accessible sur [gitlab](https://gitlab.com/avernois/degaine).

[![Dégaine](/assets/images/degaine/degainex2.jpg)](/assets/images/degaine/degainex2.jpg)


### Fonctionnalités
* Décompte du temps en minutes, de 0 à 99 minutes.
* Affichage identique des 2 côtés.
* Changement de couleur pour signaler 50%, 90% et 100% du temps écoulés.
* Choix du côté d'affichage (les 2 par défaut).
* arduino complet accessible en ouvrant le boitier.
* un unique bouton, qu'on pousse et qu'on tourne.
* (peut-être) plus à venir ?

### Détails techniques
[![By Kevin Lee](/assets/images/degaine/degaine_320.jpg)](/assets/images/degaine/degaine.jpg){: .right-image}
* taille: 152x100x33mm[^3].
* poids : 195g[^3].
* alimentation (non fournie) : 5v, 500mA, micro-USB.
* le boitier, le bouton et la structure interne sont en bois[^13]. Le bois est poncé, mais reste nature (pas de vernis/peinture/protection).
* le diffuseur est en papier.

## Commander
Le prix est presque libre : il y a un prix minimum fixé à {{ page.prix_mini_ttc }}€ TTC ({{ page.prix_mini_ht }}€ HT) qui correspond à peu près au coût des éléments pour le produire plus les frais de port en France.

Le prix conseillé c'est {{ page.prix_conseil_ttc }}€ TTC ({{ page.prix_conseil_ht }}€ HT).

### Par carte bancaire
Fixer votre prix (TTC) dans le formulaire ci-dessous, appuyer sur le bouton "Payer" et c'est parti :
vous serez redirigez vers la page de paiement.

Si tout se passe bien, vous serez ensuite redirigé vers une page indiquant le succès de la commande[^2].

<!-- Modal -->
<div class="modal fade" id="loadMe" tabindex="-1" role="dialog" aria-labelledby="loadMeLabel">
  <div class="modal-dialog modal-sm" role="document">
    <div class="modal-content">
      <div class="modal-body text-center">
        <div class="spinner-border" role="status">
          <span class="sr-only">Loading...</span>
        </div>
        <div class="loader-txt">
          <p>Vous allez être redirigé vers la page de paiment. Merci de patienter.<br><br>
          </p>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="form-row align-items-center">
  <div class="col-auto">
    <label class="sr-only" for="amount">Montant</label>
    <div class="input-group mb-2">
      <input type="number" class="form-control" id="amount" value="{{ page.prix_conseil_ttc }}">
      <div class="input-group-append">
        <div class="input-group-text">€</div>
      </div>
      <div class="invalid-tooltip" id="error"></div>
    </div>
  </div>
  <div class="col-auto">
    <button type="submit" class="btn btn-primary mb-2" id="donate">Payer</button>
  </div>
</div>

### Par chèque ou virement
Le paiement par carte est le moyen à privilégié.

Cela dit si vous le souhaitez, vous pouvez payer par chèque ou virement, pour cela, il suffit de [m'envoyer un petit message](/contact/).

### En espèce

Le paiement en espèce n'est pas possible[^9].

## Après la commande

Une fois le paiement effectué, je vous enverrais un e-mail avec la facture correspondante ainsi que des informations sur l'état de la réalisation.
Cet envoi est manuel, cela peut prendre quelques jours pour que ça arrive. Un peu de patience :)

Si vous le souhaitez, vous pourrez être consulté[^10] sur les choix qui restent à faire autant sur le design, que l'évolution des fonctionnalités.

## Délai - fabrication - livraison

### envoi des premières commandes

Les comptez à rebours sont fabriqués par mes soins à la demande.
Mon objectif est de pouvoir faire les premiers envois mi-mai 2019(1 mois et demi après les premières commandes).
Ce temps est nécessaire pour finaliser les pcbs[^11] et recevoir les composants[^12].

### livraison

Par défaut, l'idée est de vous envoyer votre dégaine par colis postal. Les frais de port pour la France sont inclus dans le prix minimum.

Si vous êtes (de passage) sur Toulouse, je serais ravi de vous le remettre en main propre autour d'un café/thé/autres si ça vous tente.

Si vous souhaitez une livraison en dehors de France ou par un transporteur différent, pensez à prendre cela en compte dans le prix (le colis devrait être autour de 300g).

## Évolution - mise à jour

Il n'est pas prévu de système de mise à jour automatique ou trivialement simple.

Cela dit, dégaine est basé sur un arduino nano complet (à base d'Atmega328) accessible en ouvrant le boitier.
Ainsi avec des compétences simples en développement logiciel[^6] vous devriez pouvoir le mettre à jour, voir faire évoluer le système si vous le souhaitez.

---

[^1]: elles seront utilisées pour l'établissement de la facture. Vous pourrez changer plus tard.
[^2]: le cas contraire, vous serez redirigé vers une page expliquant le problème.
[^3]: ces informations sont à titre indicatif, le design n'étant pas définitif il peut y avoir des légères variations.
[^4]: et dans ce cas, il s'arrête immédiatement, ce qui n'est pas très utile :)
[^5]: au moment où j'écris ces lignes, les couleurs passe vert à bleu puis jaune et enfin rouge lorsque le temps est écoulé. Elles sont susceptibles de changer pour des couleurs améliorant le contraste afin que les changements puissent être perçu par le plus grand nombre.
[^6]: comme installer [PlatformIO](https://platformio.org/) ou récuperer le code source depuis le dépot [gitlab](https://https://gitlab.com/avernois/degaine).
[^7]: le paiement et vos informations bancaires sont traitées par le service Stripe, elles ne me sont jamais accessible.
[^8]: le boitier est réaliser à la découpeuse laser. Seuls la fabrication des pcbs est externalisée.
[^9]: je n'ai pas envie d'avoir à gérer une caisse dans ma comptabilité :)
[^10]: il s'agit vraiment d'une consultation pour m'aider à faire les choix. Cela ne vous garanti pas que votre choix sera celui qui sera fait.
[^11]: j'ai un prototype parfaitement fonctionnel, mais il y a des adaptations à faire.
[^12]: la plupart des composants arrivent de chine, ça prend du temps (3-5 semaines)
[^13]: actuellement contreplaqué bouleau 3mm.

<script>
  const stripe_public_token = "{{ site.stripe_public_key }}";
	var stripe = Stripe(stripe_public_token);

	function setMinimumAmountMessage() {
    document.getElementById("amount").classList.add("is-invalid")
		document.getElementById("error").innerHTML="le montant doit &ecirc;tre minimum de "+ minimum + " euros.";
  }
  function clearErrorMessage() {
    document.getElementById("amount").classList.add("is-valid")
    document.getElementById("amount").classList.remove("is-invalid")
    document.getElementById("error").innerHTML="";
  }

	const minimum = {{ page.prix_mini_ttc }};
  
  const button = document.getElementById("donate");
  button.onclick = function (e) {

    var amount = document.querySelector('input#amount').value;

    amount = amount.replace(/\$/g, '').replace(/\,/g, '');

    amount = parseFloat(amount);

		if(amount >= minimum) {
      clearErrorMessage();

      $('#loadMe').modal({backdrop: 'static', show: true});
      const fetch_params = { method: 'POST' };

			console.log("montant: " + amount);
			fetch('{{ site.pay_server }}/checkout/degaine?amount=' + amount, fetch_params)
					.then(function(response) {
            $('#loadMe').modal({backdrop: 'static', show: false});
						console.log(response)
						if(response.ok ) {
							console.log("Response wan ok !!")
							response.text().then(function(text) {
								checkout_id = text;
								console.log(text);
								stripe.redirectToCheckout({
									// Make the id field from the Checkout Session creation API response
									// available to this file, so you can provide it as parameter here
									// instead of the {{CHECKOUT_SESSION_ID}} placeholder.
									sessionId: checkout_id
								}).then(function (result) {
									// If `redirectToCheckout` fails due to a browser or network
									// error, display the localized error message to your customer
									// using `result.error.message`.
								});
							})
						} else {
							console.log("Response was not okay !");
						}
					});
		} else {
			setMinimumAmountMessage();		
		}
};

</script>
