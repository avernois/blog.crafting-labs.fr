---
layout: page
title: "Échec : une erreur est survenue lors de la transaction !"
permalink: /degaine/paymentException/
---

Oh non, lorsque le serveur a essayé de traiter votre paiement auprès de Stripe, ce dernier à retourné une erreur.

Si c'est possible, j'apprécierai que vous me [contactiez](/contact) pour me donner le détail de ce que vous faisiez pour que je puisse corriger le problème au plus vite. Merci d'avance.

Pour me contacter, les infos sont sur la [page de contact](/contact).

