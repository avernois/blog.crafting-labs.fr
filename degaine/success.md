---
layout: page
title: Succès !
permalink: /degaine/success/
---

Yeah, votre paiement a réussi. Merci.

Je vous recontacte très prochainement pour vous donner les infos sur la suite des opérations.